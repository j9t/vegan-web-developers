# Vegan Web Developers

⚠️ The vegan web developers list is available under [j9t.gitlab.io/vegan-web-developers](https://j9t.gitlab.io/vegan-web-developers/).

This is an overview of [vegan](https://en.wikipedia.org/wiki/Veganism) web developers. Every vegan web developer may join: Add yourself directly [to the HTML file](https://gitlab.com/j9t/vegan-web-developers/-/blob/master/index.html) and [submit a merge request](https://gitlab.com/j9t/vegan-web-developers/-/merge_requests/new). (Please sort by last name, link only a personal website as well as either your Mastodon or Twitter account, and stick with the existing structure, including no use of [optional markup](https://meiert.com/en/blog/optional-html/).) Alternatively, ask to be added, by [filing an issue here](https://gitlab.com/j9t/vegan-web-developers/-/issues/new) or [on GitHub](https://github.com/j9t/vegan-web-developers-forum/issues/new/choose).

The list is based on an honor system. If you’re not a vegan and not a web developer, don’t join, or take yourself off the list again once you stopped being vegan or a web developer.